---
layout: page
title: CaReRa- Content Based Case Retrieval in Radiological Databases
description:  An interdiscipliary project where the liver and its diseases are modeled from a radiological perspective 
img: /assets/img/Lico-DataProperty-DL.png
---

<div class="img_row">
    <img class="col three" src="{{ site.baseurl }}/assets/img/project-1-overview.jpg" alt="semantic liver representation overview" title="Case Based Liver Patient Platform"/>
</div>
<div class="col three caption">
The overview of a case based liver diagnostic support system that utilizes the LiCO ontology
</div>
<div class="img_row">
    <img class="col three" src="{{ site.baseurl }}/assets/img/ontological-liver-concepts.jpg" alt="Liver Concepts in LiCO ontology" title="Liver Concepts in LiCO Ontology"/>
</div>
<div class="col three caption">
The main concepts of the liver specified in the LiCO ontology that utilizes many stardard ontologies in the medical domain.
</div>


<div>
del Mar Roldán-García, María, Suzan Uskudarli, Neda B. Marvasti, Burak Acar, and José F. Aldana-Montes <a href="https://doi.org/10.1016/j.eswa.2018.02.001">Towards an ontology-driven clinical experience sharing ecosystem: Demonstration with liver cases</a> Expert Systems With Applications 101 (2018): 176-19
</div>
